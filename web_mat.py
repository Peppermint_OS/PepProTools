#!/usr/bin/env python3

from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtWebEngineWidgets import *
import sys

class MainWindow(QMainWindow):
#set up the main window and the url address. 
    def __init__(self, *args, **kwargs):
        super(MainWindow,self).__init__(*args, **kwargs)
        self.setWindowTitle("[matrix]")
        self.setWindowIcon(QIcon('/opt/pypep/mt.png'))  
        self.browser = QWebEngineView()
        self.browser.setUrl(QUrl("https://matrix.to/#/!JhPtEbNexzFaoOkvGp:matrix.org?via=matrix.org"))
        self.setCentralWidget(self.browser)
        self.show()

app = QApplication(sys.argv)
window = MainWindow()

app.exec_()
