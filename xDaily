#!/bin/bash

PROGNAME="xDaily"
[ "$USER" != "root" ] &&
   echo -e "\t$PROGNAME must be run as root. \n\tOr with \`sudo $PROGNAME\` ." && exit

### Our options are going to be
#    = "full output to the terminal, no stopping."
# -i = "interactive"
# -q = "interactive but suppressed output"
OPTIND="0"
for i in  _quiet _interactive ; do unset -v $i ; done

while getopts 'iq' OPTION; do
    case "$OPTION" in
      i)
    _interactive="yes"
    echo -e " Entering interactive mode\n"
    break
      ;;

      q)
    _quiet="yes"
    echo -e " Entering interactive mode, without verbose output.\n"
    break
      ;;

      ?)
    echo "Usage: $(basename ${PROGNAME}) [-i = interactive] [-q = suppressed ouput]"
    return 1
      ;;
  esac
done

[ "$(which nala)" ] && PkgMgr="nala" || PkgMgr="apt"

see_it() {
    [ "$_interactive" = "yes" ] && ( read -n1 -p " $_msg ? \"Y/n\" " answ
    [ -z $answ ] || [ "$answ" = "y" ] || [ "$answ" = "Y" ] && echo &&
    do_it &&
    echo -e " $_msg - Completed.\n" ||
    echo -e "\r $_msg - Skipped.\n" )
}

no_see() {
    [ "$_quiet" = "yes"       ] && ( read -n1 -p " $_msg ? \"Y/n\" " answ
    [ -z $answ ] || [ "$answ" = "y" ] || [ "$answ" = "Y" ] && echo &&
    do_it > /dev/null &&
    echo -e " $_msg - Completed.\n" ||
    echo -e "\r $_msg - Skipped.\n" )
}

run_it() {
    [ "$_interactive" != "yes" ] && [ "$_quiet" != "yes" ] && (
    echo -e "\n $_msg ." &&
    do_it &&
    echo -e " $_msg - Completed." )
}

# Begin xDaily command functions
_update() {
      _msg="Check apt repositories for Updates"
      do_it() {
        [ "$_quiet" != "yes" ] &&
        $PkgMgr update        ||
        $PkgMgr update 2>&1 >/dev/null
}
    see_it
    no_see
    run_it
}

_upgradable() {
      _msg="See upgradable packages"
      do_it() {
        [ "$_quiet" != "yes" ] &&
        $PkgMgr list --upgradable  ||
        $PkgMgr list --upgradable 2>&1 >/dev/null
}
    see_it
#   no_see
#   run_it
}

_upgrade() {
      _msg="Install available updated packages"
      do_it() {
        [ "$_quiet" != "yes" ] &&
        $PkgMgr upgrade       ||
        $PkgMgr upgrade 2>&1 >/dev/null
}
    see_it
    no_see
    run_it
}

_apt_clean() {
      _msg="Remove unneccessary packages from APT cache"
      do_it() {
        [ "$_quiet" != "yes" ] &&
        $PkgMgr clean         ||
        $PkgMgr clean 2>&1 >/dev/null
}
    see_it
    no_see
    run_it
}

_autoclean () {
      _msg="Remove unavailable entries from APT cache"
      do_it() {
        [ "$_quiet" != "yes" ] &&
        $PkgMgr autoclean     ||
        $PkgMgr autoclean 2>&1 >/dev/null
}
    see_it
    no_see
    run_it
}

_autoremove() {
      _msg="Remove old dependencies not required by the system"
      do_it() {
        [ "$_quiet" != "yes" ] &&
        $PkgMgr autoremove    ||
        $PkgMgr autoremove 2>&1 >/dev/null
}
    see_it
    no_see
    run_it
}

_clear_thumbnails() {
      _msg="Clear browser thumbnail caches"
      do_it() {
        for i in ".thumbnails" ".cache/thumbnails" ; do
           for j in "*/*.png" "*/*/*.png" ; do
            [ "$_quiet" != "yes" ]  &&
            rm -v /home/${SUDO_USER}/${i}/${j} 2>/dev/null ||
                rm /home/${SUDO_USER}/${i}/${j} 2>/dev/null
           done
        done ; true
          }
    see_it
    no_see
    run_it
}

_clear_recents() {
      _msg="Clear the \"Recently Used\" list in FireFox"
      do_it() {
        dd bs=1 count=1 status=none if=/dev/null of=/home/${SUDO_USER}/.local/share/recently-used.xbel
        chown ${SUDO_USER} /home/${SUDO_USER}/.local/share/recently-used.xbel
}
    see_it
    no_see
    run_it
}

_rbranding() {
      _msg="Reconfirm Peppermint Branding in os-release"
      do_it() {
        diff -q /opt/pepconf/os-release /usr/lib/os-release || cp /opt/pepconf/os-release /usr/lib/os-release
        diff -q /opt/pepconf/os-release /etc/os-release     || cp /opt/pepconf/os-release /etc/os-release
          }
    see_it
    no_see
    run_it
}

_ssd_trimfs() {
      _msg="For SSDs: trim eligible ext2/3/4 filesystems"
      do_it() {
        for mnt in $(grep -E "(ext2|ext3|ext4)" /etc/mtab | cut -f2 -d" ")
            do fstrim ${mnt} &>/dev/null &&
            echo -e " Completed fstrim for \"${mnt}\"" ||
            echo -e " No fstrim required for \"${mnt}\""
            done
          }
        see_it
        no_see
        run_it
}


_udcache() {
      _msg="Caching icons at /usr/share/icons/"
      do_it() { update-icon-caches /usr/share/icons/* ;}
        see_it
        no_see
        run_it
}


_update
_upgradable
_upgrade
_apt_clean
if [ "$PkgMgr" = "apt" ]; then _autoclean ; fi
_autoremove
_clear_thumbnails
_clear_recents
_udcache
_rbranding
_ssd_trimfs
_ptools

[ "$_interactive" = "yes" ] &&
 read -n1 -p " Press any key to continue ... " answ
echo

