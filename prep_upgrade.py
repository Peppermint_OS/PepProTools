"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* Prep the user's computer to get upgraded
*
"""
import os
import subprocess
import shutil
import tkinter as tk
import ttkbootstrap as ttk
import upgradeconf



def ubase():
    """Check to see what base is being loaded"""
    if os.path.exists("/etc/devuan_version"):
        pepupdate.title(upgradeconf.PREP_DEVUAN_TITLE)
    elif os.path.exists("/etc/debian_version"):
        pepupdate.title(upgradeconf.PREP_DEBIAN_TITLE)

def start_upgrade():
    """ Open the suggested packages """
    os.system('sudo python3 /opt/pypep/upgrade.py')


def center_screen():
    """ gets the coordinates of the center of the screen """
    screen_width = pepupdate.winfo_screenwidth()
    screen_height = pepupdate.winfo_screenheight()
    # Coordinates of the upper left corner of the window to make the window
    # appear in the center
    x_cordinate = int((screen_width / 2) - (WINDOW_WIDTH / 2))
    y_cordinate = int((screen_height / 2) - (WINDOW_HEIGHT / 2))
    pepupdate.geometry("{}x{}+{}+{}".format(WINDOW_WIDTH,
                   WINDOW_HEIGHT, x_cordinate, y_cordinate))


def apt_func():
    """ Will clean the system and removed the un-needed calamares"""
    sudo_apt_var = 'sudo apt '
    link = ' && '
    clean = sudo_apt_var + 'autoremove -y'
    inst_lua = sudo_apt_var + 'install luakit -y'
    remove_calamares = sudo_apt_var + 'remove calamares -y'
    remove_calamares_settings = sudo_apt_var + 'remove calamares-settings-debian -y'
    full_cmd = inst_lua + link + remove_calamares + link + remove_calamares_settings + link + clean
    with subprocess.Popen(full_cmd, shell=True, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT) as sp:
                              for line in iter(sp.stdout.readline, b''):
                                  print(line.rstrip()
                                  )
    print("Prep Completed Ready For Upgrade!")


def copy_func():
    """ This will corect the legacy peptools"""
    src_pmos = '/opt/pypep/pmostools/peptools'
    prep_p = upgradeconf.prep_path
    panel_p = upgradeconf.panel_path
    des_pmos = upgradeconf.spath
    src_path_base = '/opt/pypep/'
    des_path_base = '/usr/local/bin/'
    des_path_apps = '/usr/share/applications/'
    sudo_cp_var =  'sudo cp '
    link = ' && '
    kumo_exe = sudo_cp_var + src_path_base + 'kumo ' + des_path_base + 'kumo'
    hub_exe = sudo_cp_var + src_path_base + 'hub ' + des_path_base + 'hub'
    welcome_exe = sudo_cp_var + src_path_base + 'welcome ' + des_path_base + 'welcome'
    kumo_app = sudo_cp_var + src_path_base + 'kumo.desktop ' + des_path_apps + 'kumo.desktop'
    hub_app = sudo_cp_var + src_path_base + 'Pephub.desktop ' + des_path_apps + 'Pephub.desktop'
    welcome_app = sudo_cp_var + src_path_base + 'Welcome.desktop ' + des_path_apps + 'Welcome.desktop'
    pep_tools = 'mkdir ' + prep_p + 'pmostools' + link + 'cp -r ' + src_pmos + ' ' + des_pmos
    hub_panel = 'cp ' + src_path_base + '16158061764.desktop ' + panel_p + '16158061764.desktop'
    full_cmd = kumo_exe + link + hub_exe + link + welcome_exe + link + kumo_app + link + hub_app + link + welcome_app + link + pep_tools + link + hub_panel
    with subprocess.Popen(full_cmd, shell=True, stdout=subprocess.PIPE,
                          stderr=subprocess.STDOUT) as sp:
                              for line in iter(sp.stdout.readline, b''):
                                  print(line.rstrip())

    apt_func()







# setting up window
pepupdate = upgradeconf.bbstyle
pepupdate.resizable(False, False)
WINDOW_HEIGHT = 150
WINDOW_WIDTH = 350


# Set the window icon
pepupdate.tk.call('wm', 'iconphoto', pepupdate._w, tk.PhotoImage(
    file='/usr/share/pixmaps/peppermint-old.png'))

prep_title = ttk.Label(pepupdate,
                       text="This tool will help do the following:",
                       bootstyle="warning"
                      )
prep_title.place(x=10, y=10)

prep_peptools = ttk.Label(pepupdate,
                          text="- Upgrade the Peppermint Tools.",
                         )
prep_peptools.place(x=10, y=30)

prep_dep = ttk.Label(pepupdate,
                          text="- Apply Peppermint specific dependencies.",
                         )
prep_dep.place(x=10, y=50)

prep_rmv = ttk.Label(pepupdate,
                          text="- Remove no longer needed Peppermint Settings.",
                         )
prep_rmv.place(x=10, y=70)


btnprep = ttk.Button(
    pepupdate,
    text="Prepare System",
    cursor="hand2",
    bootstyle="light-outline",
    width=15,
    command=copy_func

    )
btnprep.place(x=10, y=110)

btnstart = ttk.Button(
    pepupdate,
    text="Begin Upgrade",
    cursor="hand2",
    bootstyle="light-outline",
    width=15,
    command=start_upgrade
    )
btnstart.place(x=180, y=110)



ubase()
center_screen()
pepupdate.mainloop()
