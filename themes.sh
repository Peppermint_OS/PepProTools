#!/bin/bash

_extras=themes
_repo="Peppermint_OS/PepThemes"

###########################################
# Quietly have wget -q0- "pipe" its output directly to tar.
# Use tar -xvz to decompress, list and untar the DL file
# to the symlink 'pepthemes' in /usr/share, on the fly.
# Leaving no files to remove and no permission changes required.
###########################################

echo -e "\n Downloadng Additional ${_extras} to /usr/share/"
wget https://codeberg.org/${_repo}/archive/main.tar.gz --show-progress -qO - |
  tar -xz -C /usr/share 2>/dev/null

echo -e "\n Additional Themes downloaded and installed.\n"

read -n1 -p " Process Completed. Press any key to close this dialog." answ

# Go back to the Extras
python3 /opt/pypep/pge.py &

