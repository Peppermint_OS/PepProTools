"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This is for the Hub GUI
"""
import tkinter as tk
import tkinter.ttk as ttk
import subprocess
import os
from tendo import singleton
import bsconf
import hubconf
import actions

# This will get the logged in user
gusr = os.getlogin()
spath = "/home/" + gusr + "/.local/share/pmostools/peptools"

# setting up window
pehub = bsconf.bbstyle
WINDOW_HEIGHT = 420
WINDOW_WIDTH = 740
pehub.resizable(False, False)
pehub.title('Peppermint Hub')
pehub.tk.call('wm', 'iconphoto', pehub._w,
              tk.PhotoImage(
                  file=spath + '/images/peppermint-hub.png'))


def center_screen():
    """ gets the coordinates of the center of the screen """
    screen_width = pehub.winfo_screenwidth()
    screen_height = pehub.winfo_screenheight()
    # Coordinates of the upper left corner of the window to make the window
    # appear in the center
    x_cordinate = int((screen_width / 2) - (WINDOW_WIDTH / 2))
    y_cordinate = int((screen_height / 2) - (WINDOW_HEIGHT / 2))
    pehub.geometry("{}x{}+{}+{}".format(WINDOW_WIDTH,
                   WINDOW_HEIGHT, x_cordinate, y_cordinate))


def runxd():
    """Run the bash, x-terminal-emulator will attempt
       to run the default terminal emulator of the system
     """
    subprocess.Popen(["x-terminal-emulator", "-e", "sudo", "xDaily", "-i"])



def gnmhandle():
    """ handle the Gnome store"""
    check_path = '/usr/bin/gnome-software'
    if os.path.exists(check_path):
        btngs.grid(
            row=2,
            column=0,
            ipadx=10,
            ipady=10,
            padx=10,
            pady=10,
            sticky='nesw')
        lblgs.grid(row=3, column=0, padx=0, pady=5)
    else:
        btngsw.grid(
            row=2,
            column=0,
            ipadx=10,
            ipady=10,
            padx=10,
            pady=10,
            sticky='nesw')
        lblgsw.grid(row=3, column=0, padx=0, pady=5)


def fphandle():
    """ Handle Flathub store """
    check_flatpak_path = '/usr/bin/flatpak'
    if os.path.exists(check_flatpak_path):
        btnsp.grid(
            row=0,
            column=0,
            ipadx=10,
            ipady=10,
            padx=10,
            pady=10,
            sticky='nesw')
        lblsp.grid(row=1, column=0)
        check_gnome_path = '/usr/bin/gnome-software'
        if os.path.exists(check_gnome_path):
            btnfp.destroy()
            lblfp.destroy()
        else:
            lblfp.grid(row=3, column=1, padx=0, pady=5)
            btnfp.grid(
                row=2,
                column=1,
                ipadx=10,
                ipady=10,
                padx=10,
                pady=10,
                sticky='nesw')
    else:
        lblfp.grid(row=3, column=1, padx=0, pady=5)
        btnfp.grid(
            row=2,
            column=1,
            ipadx=10,
            ipady=10,
            padx=10,
            pady=10,
            sticky='nesw')



def sbphandle():
    """ Hanlde Snap Store """
    check_snap_path = '/usr/bin/gnome-software'
    if os.path.exists(check_snap_path):
        btnsw.destroy()
        lblsw.destroy()
        check_gnome_path = '/usr/bin/gnome-software'
        if os.path.exists(check_gnome_path):
            btnss.grid(
                row=2,
                column=2,
                ipadx=10,
                ipady=10,
                padx=10,
                pady=10,
                sticky='nesw')
            lblss.grid(row=3, column=2, padx=0, pady=5)
    else:
        if os.path.exists(check_snap_path):
            lblss.destroy()
            btnss.destroy()
            btnsw.grid(
                row=2,
                column=2,
                ipadx=10,
                ipady=10,
                padx=10,
                pady=10,
                sticky='nesw')
            lblsw.grid(row=3, column=2, padx=0, pady=5)
        else:
            btnsw.grid(
            row=2,
            column=2,
            ipadx=10,
            ipady=10,
             padx=10,
            pady=10,
            sticky='nesw')
            lblsw.grid(row=3, column=2, padx=0, pady=5)


def xsetting_handle():
    """ Check to see what the desktop session is
        If its not xfce do not use the xfce
        setting button or user groups
    """
    check = os.environ.get('DESKTOP_SESSION')
    using = check
    if using == "xfce":
        # xfce settings
        btnxf = ttk.Button(
        gfs,
        text="XF",
        cursor="hand2",
        bootstyle="dark-outline",
        image=hubconf.xf,
        command=actions.osettingsmgr)
        lblxf = ttk.Label(gfs, text=hubconf.lxf)
        btnxf.grid(
        row=4,
        column=2,
        ipadx=10,
        ipady=10,
        padx=10,
        pady=10,
        sticky='nesw')
        lblxf.grid(row=5, column=2, padx=0, pady=5)
        # User Groups
        btnug = ttk.Button(
        gfs,
        text="UG",
        cursor="hand2",
        bootstyle="dark-outline",
        image=hubconf.ug,
        command=actions.ssug)
        btnug.grid(
        row=4,
        column=1,
        ipadx=10,
        ipady=10,
        padx=10,
        pady=10,
        sticky='nesw')
        lblug.grid(row=5, column=1)
    else:
        pass


def debianbase():
    """Bring functions together"""
    fphandle()
    gnmhandle()
    sbphandle()


def vdecide():
    """Decide Version logo to use"""
    if os.path.exists("/etc/devuan_version"):
        devuanbase()
    elif os.path.exists("/etc/debian_version"):
        debianbase()


# makes sure only a single instance is running
me = singleton.SingleInstance()
X = 1
# Frame that manages system tools
gfr = tk.Frame(pehub)
gfr.grid(row=0, column=0, columnspan=2, ipadx=5, ipady=5, padx=10, pady=5)

# Border fram for system tools
gfs = ttk.Labelframe(gfr, bootstyle="warning", text="General System Tools")
gfs.grid(row=0, column=0)

# frame that manages the software tools
sfr = tk.Frame(pehub)
sfr.grid(
    row=0,
    column=3,
    columnspan=2,
    ipadx=5,
    ipady=5,
    padx=10,
    pady=5,
    sticky='n')

# Border frame for software
sfs = ttk.Labelframe(sfr, bootstyle="warning", text="Software Tools")
sfs.grid(row=0, column=0)


###########################
###General System Tools####
###Start###################
btnnc = ttk.Button(
    gfs,
    text="Network",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.nc,
    command=actions.nhnm)
btnpr = ttk.Button(
    gfs,
    text="Printers",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.pr,
    command=actions.nhpr)
btndu = ttk.Button(
    gfs,
    text="DU",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.du,
    command=actions.ssdu)

btnpa = ttk.Button(
    gfs,
    text="PA",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.pa,
    command=actions.nhpav)
btnxd = ttk.Button(
    gfs,
    text="XD",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.xd,
    command=runxd)
btnum = ttk.Button(
    gfs,
    text="UM",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.um,
    command=actions.sssi)

lblnc = ttk.Label(gfs, text=hubconf.lnc)
lblpr = ttk.Label(gfs, text=hubconf.lpr)
lbldu = ttk.Label(gfs, text=hubconf.ldu)
lblug = ttk.Label(gfs, text=hubconf.lug)
lblpa = ttk.Label(gfs, text=hubconf.lpa)
lblxd = ttk.Label(gfs, text=hubconf.lxd)
lblhb = ttk.Label(gfs, text=hubconf.lhb)
lblsi = ttk.Label(gfs, text=hubconf.lsi)
lblum = ttk.Label(gfs, text=hubconf.lum)

btnnc.grid(
    row=0,
    column=0,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblnc.grid(row=1, column=0)
btnpr.grid(
    row=0,
    column=1,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblpr.grid(row=1, column=1)
btndu.grid(
    row=0,
    column=2,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lbldu.grid(row=1, column=2)
lblhb.grid(row=3, column=0)


btnpa.grid(
    row=4,
    column=0,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblpa.grid(row=5, column=0, padx=0, pady=5)
btnum.grid(
    row=2,
    column=1,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblum.grid(row=3, column=1, padx=0, pady=5)

btnxd.grid(
    row=2,
    column=2,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblxd.grid(row=3, column=2)


###########################
###General System Tools####
###End#####################

def hblock():
    """ Take care of Hblock"""
    file_check= open('/etc/hosts', 'r')
    read_file = file_check.read()

    word = 'Blocked domains: 0'
    if word in read_file:
        btnhbon = ttk.Button( gfs,
                              text="hbon",
                              cursor="hand2",
                              bootstyle="dark-outline",
                              image=hubconf.hbon,
                              command=hboff
                              )
        btnhbon.lower()
        btnhboff = ttk.Button( gfs,
                               text="hboff",
                               cursor="hand2",
                               bootstyle="dark-outline",
                               image=hubconf.hboff,
                               command=hbon
                              )
        btnhboff.grid( row=2,
                       column=0,
                       ipadx=10,
                       ipady=10,
                       padx=10,
                       pady=10,
                       sticky='nesw'
                       )
    else:
        btnhboff = ttk.Button( gfs,
                                text="hboff",
                                cursor="hand2",
                                bootstyle="dark-outline",
                                image=hubconf.hboff,
                                command=hbon
                                )
        btnhboff.lower()
        btnhbon = ttk.Button( gfs,
                              text="hbon",
                              cursor="hand2",
                              bootstyle="dark-outline",
                              image=hubconf.hbon,
                              command=hboff
                            )
        btnhbon.grid(row=2,
                     column=0,
                     ipadx=10,
                     ipady=10,
                     padx=10,
                     pady=10,
                     sticky='nesw'
                     )


def hbon():
    """ Turn on hblock """
    subprocess.Popen(["x-terminal-emulator", "-e", "hblock", "exit"])
    hblock()



def hboff():
    """ Turn off hblock """
    subprocess.Popen(["x-terminal-emulator", "-e", "hblock", "-S",
                      "none", "-D", "none", "exit"])
    hblock()




###########################
######Software Start#######
###########################
btnsp = ttk.Button(
    sfs,
    text="sp",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.sp,
    command=actions.packages)
btnah = ttk.Button(
    sfs,
    text="ah",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.ah,
    command=actions.ssai)
btnsc = ttk.Button(
    sfs,
    text="sc",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.sc,
    command=actions.ssspm)
btnss = ttk.Button(
    sfs,
    text="ss",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.ss,
    command=actions.ssst)
btnsw = ttk.Button(
    sfs,
    text="ss",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.ss,
    command=actions.ssss)
btngs = ttk.Button(
    sfs,
    text="gs",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.gs,
    command=actions.ssgn)
btngsw = ttk.Button(
    sfs,
    text="gsw",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.gs,
    command=actions.ssgns)
btnfp = ttk.Button(
    sfs,
    text="fp",
    cursor="hand2",
    bootstyle="dark-outline",
    image=hubconf.fp,
    command=actions.ssfh)


lblsp = ttk.Label(sfs, text=hubconf.lsp)
lblah = ttk.Label(sfs, text=hubconf.lah)
lblsc = ttk.Label(sfs, text=hubconf.lsc)
lblss = ttk.Label(sfs, text=hubconf.lss)
lblsw = ttk.Label(sfs, text=hubconf.lssw)
lblgs = ttk.Label(sfs, text=hubconf.lgs)
lblgsw = ttk.Label(sfs, text=hubconf.lgsw)
lblfp = ttk.Label(sfs, text=hubconf.lfp)

btnsp.grid(
    row=0,
    column=0,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblsp.grid(row=1, column=0)
btnah.grid(
    row=0,
    column=1,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblah.grid(row=1, column=1)
btnsc.grid(
    row=0,
    column=2,
    ipadx=10,
    ipady=10,
    padx=10,
    pady=10,
    sticky='nesw')
lblsc.grid(row=1, column=2)


###Functions needed for the software sorting###

def devuanbase():
    """set up for a Devuan Build"""
    lblss.destroy()
    btnss.destroy()
    btnfp.grid(
        row=2,
        column=1,
        ipadx=10,
        ipady=10,
        padx=10,
        pady=10,
        sticky='nesw')
    lblfp.grid(row=3, column=1, padx=0, pady=5)
    fphandle()
    btnsp.grid(
        row=0,
        column=0,
        ipadx=10,
        ipady=10,
        padx=10,
        pady=10,
        sticky='nesw')
    lblsp.grid(row=1, column=0)
    gnmhandle()



hblock()
xsetting_handle()
# call Center screen
center_screen()
# start the version process
vdecide()
# set the block status
# run the applictaion
pehub.mainloop()
