"""
* Author: "PepDebian(peppermintosteam@proton.me)
*
* License: SPDX-License-Identifier: GPL-3.0-or-later
*
* This file is used to keep functions that are used for the Welcome
* Screen
"""
import os.path


# This will get the logged in user
gusr = os.getlogin()
spath = "/home/" + gusr + "/.local/share/pmostools/peptools"

def suggested_packages():
    """ Open the suggested packages """
    os.system('python3 ' + spath + '/suggested.py')

def hub():
    """Open the Hub"""
    os.system('hub')


##SSB's###

def pep_docs():
    """Open Pep Docs"""
    os.system('luakit -U https://peppermint_os.codeberg.page/html/ &'
              )

# Community Section


def mastodon():
    """Open Mastodon"""
    os.system('luakit -U https://fosstodon.org/@peppermintos &'
              )


def code_berg():
    """Open Codeberg"""
    os.system('luakit -U https://codeberg.org/Peppermint_OS & '
              )


def matrix():
    """Open Matrix"""
    os.system('luakit -U https://matrix.to/#/!JhPtEbNexzFaoOkvGp:matrix.org?via=matrix.org & '
              )


def source_forge():
    """Open Sourceforge"""
    os.system('luakit -U https://sourceforge.net/p/peppermintos/pepos/ & '
              )


def build_date():
    """Open BuildDate wiki"""
    os.system('luakit -U https://sourceforge.net/p/peppermintos/pepwiki/BuildDate/ & '
              )
