Copyright (C) 2022 Peppermint OS.

# Pep_Pro_Tools

## Peppermint Tools

These are the Peppermint tools. Anything that you see that is on the
welcome screen and thePeppermint hub is maintained in this repository.

These tools are distributed under the terms of the GNU General Public
License Version 3 or any later version (SPDX: *GPL-3.0-or-later*).

## Contributions and Bug Reports

Contributions are accepted as CodeBerg pull requests or via email (`git
send-email` patches). Any problem may also be reported through CodeBerg issues
page or by contacting: peppermintosteam@proton.me

## Requirements

*Peppermint Tools* requires the following for installation and usage:

- Python 3.0 or higher
- python3-pip
- PyQt5-QtSql 
- PyQt5
- PyQtWebengine
- PyQt5-sip
- GitPython
- tendo
- python-crontab
- ttkbootstrap
- favicon
- bs4

## Social Verification
You can find us on the 
<a rel="me" href="https://fosstodon.org/@peppermintos">Mastodon</a>,  follow us there